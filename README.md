# RenameMe

Brieuc DANIEL -
brieuc@bdaniel.fr -
Student Engineer at Ecole Supérieure d'Ingénieurs de Rennes (Specialty: Image Processing)

### Install Requirements

*Currently only on UNIX Systems*

1. Exiv2 (v >= 0.25)
 Available at http://www.exiv2.org/download.html
 This library is used in order to access pictures' Metadatas

2. Boost (v >= 1.61)
 Available at http://www.boost.org/users/download/
 This library is used to manage files

3. Qt (v >= 5.6)
 Available at https://www.qt.io/download/
 Qt is used for the application's GUI

### Compiling and Execution

After successfully installing the different libraries, you may be able to generate and compile files as follows:

`cd RenameMe
mkdir build
cmake ..
make
./RenameMe`

ENJOY !
